Vue.component('component-a', {
    props: {
        bigheader: {
            type: String,
            required: true
        }
    },
    data: function () {
        return {
            counter: 0
        }
    },
    template:
        /*html*/
        `<div>
    <h3>{{bigheader}}</h3
    <button v-on:click="counter++">You clicked me {{ counter }} times.</button>
    </div>`
})

var app = new Vue({
    el: '#app',
    data: {
        title: 'test',
        message: 'Vue 3 Playground',
        reviews: []
    },
    methods:{
        saveReview(review){
            console.log('reviewparent',review)
            this.reviews.push(review)
            console.log('reviews',this.reviews)
        }
    },
    computed: {
        reverseWord: function () {
            return this.message.split('').reverse().join('')
        }
    },
    components: {
        'my-button': counterClick,
        'review-form': ReviewForm,
        'review-list': ReviewList
    }
})

