var counterClick={
    data:function(){
        return{
            counter:0
        }
    },
    template: 
    /*html*/
    `<button v-on:click="counter++">You clicked me {{ counter }} times in local component.</button>`
}