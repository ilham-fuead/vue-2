ReviewForm={
    data:function(){
        return{
            nama:'',
            rating:null,
            review:''
        }
    },
    template:
    /*html*/
    `
    <form @submit.prevent="onSubmit">
    <p>Nama:<input v-model="nama"></p>
    <p>Rating:<select v-model="rating">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select></p>
    <p>Review: <textarea v-model="review"></textarea></p>
    <button type="submit">Save Review</button>
    </form>
    `,
    methods:{
        onSubmit:function(){
            var review={
                nama: this.nama,
                rating: this.rating,
                review: this.review
            }
            console.log(review)
            this.$emit('add-review',review)

            this.nama=''
            this.rating=null
            this.review=''
        }
    }
}