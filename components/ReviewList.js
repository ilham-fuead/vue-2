var ReviewList={
    props:{
        reviews:{
            type:Array,
            default:[]
        }
    },
    template:
    /*html*/
    `
    <ul>
    <li v-for="(review,index) in reviews" :key="index">
    {{review.nama}} give {{review.rating}} star. 
    <br/>{{review.review}}</li>
    </ul>
    `
}